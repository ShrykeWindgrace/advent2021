import           Test.Hspec            (describe, hspec, it, shouldBe)
import           Test.Hspec.Megaparsec (shouldParse)
import qualified Text.Megaparsec as TM
import qualified Day10
main :: IO ()
main = hspec $ do
    describe "Day10" $ do
        it "base5" $ do
            Day10.toDec <$> Day10.parseTest "}}]])})]" `shouldBe` Just 288957

