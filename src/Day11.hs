{-# LANGUAGE DerivingStrategies #-}

module Day11 (runDay) where
import           Control.Monad            (replicateM, unless)
import           Control.Monad.RWS.Strict (MonadWriter (tell), RWS, execRWS)
import           Data.Char                (digitToInt)
import           Data.Function            ((&))
import           Data.Functor             ((<&>))
import           Data.Map.Strict          (Map)
import qualified Data.Map.Strict          as M
import           Data.Semigroup           (Sum (Sum, getSum))
import           Lens.Micro               (_1, _2)
import           Lens.Micro.Mtl           (use, (%=), (+=))
import           Paths_advent2021         (getDataFileName)
import           Utils                    (propagate)

runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day11.txt"
    content <- readFile realpath
    let grid = parse content
    execRWS (replicateM 100 tick) () (grid, 0) & snd & getSum & print

    execRWS tick2 () (grid, 0) & fst & snd & print

data Cell = Wait Int | Flash deriving stock Eq
type Coord = (Int, Int)
type Grid = M.Map Coord Cell
type R a = RWS  () (Sum Int) (Grid, Int) a

ping :: Coord -> R [Coord]
ping p = do
    mp <- use _1
    case M.lookup p mp of
        Just (Wait x) -> if x < 9 then [] <$ (_1 %= M.insert p (Wait (x+1))) else neighs p <$ (_1 %= M.insert p Flash >> tell (Sum 1))
        _ -> pure []

rise :: R [Coord]
rise = propagate ping ((,) <$> [0..10] <*> [0..10]) -- grid size is given

cyc :: [Coord] -> R ()
cyc cs = do
    nxt <- propagate ping cs
    if null nxt then pure () else cyc nxt

tick :: R ()
tick = rise >>= cyc >> dim

check_ :: Map Coord Cell -> Bool
check_ = M.foldr' (\v acc -> acc && (v == Wait 0)) True

tick2 :: R ()
tick2 = do
    tick
    _2 += 1
    now <- check_ <$> use _1
    unless now tick2


dim :: R ()
dim = _1 %= M.map dim_

dim_ :: Cell -> Cell
dim_ Flash = Wait 0
dim_ x     = x

neighs :: Coord -> [Coord]
neighs (x,y) = [(x + dx, y + dy) | dx <- [-1, 0, 1], dy <- [-1, 0, 1], (dx, dy) /= (0, 0)]

-- parsing, ffs


-- | don't look too closely, it works
parse :: String -> Grid
parse s = s & lines <&> zip [0..] & zip [0..] >>= shuffle <&> fmap (Wait . digitToInt) & M.fromList where

    shuffle :: (Int, [(Int, a)]) -> [((Int,Int), a)]
    shuffle (x, ps) = fmap (\(y, a) -> ((x,y),a)) ps
