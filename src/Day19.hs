{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Day19 where
import           Control.Monad    ((>=>))
import           Data.List        (foldl')
import           Data.List.Extra  (splitOn, words)
import qualified Data.Map.Strict  as Map
import           Data.Maybe       (fromMaybe, isJust, mapMaybe)
import           Paths_advent2021 (getDataFileName)
import           Text.Read        (readMaybe)
import Data.Either
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day19.txt"
    content <- readFile realpath
    print 1
