{-# LANGUAGE TupleSections #-}
module Day5 (runDay) where
import qualified Data.Map.Strict      as M
import           Data.Semigroup       (Endo (Endo, appEndo))
import           Data.Void            (Void)
import           Paths_advent2021     (getDataFileName)
import           Text.Megaparsec      (Parsec, parseMaybe, some)
import           Text.Megaparsec.Char (char, digitChar, string)


runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day05.txt"
    content <- lines <$> readFile realpath
    let (Just segs) = traverse (parseMaybe parseSegment) content
    putStrLn "part1"
    print $ moreThanTwo $ mkGrid segs toLine

    putStrLn "part2"
    print $ moreThanTwo $ mkGrid segs toLine2

type Coord = (Int, Int)
type Grid = M.Map Coord Int
data Segment = Segment Coord Coord

minMax :: Int -> Int -> (Int, Int)
minMax x y = (min x y, max x y)

-- | how to insert entries on the grid
upd :: Coord -> Grid -> Grid
upd c = M.insertWith (+) c 1

mkGrid :: [Segment] -> (Segment -> [Coord]) -> Grid
mkGrid segs method = appEndo (foldMap Endo (upd <$> (segs >>= method))) M.empty

moreThanTwo :: Grid -> Int
moreThanTwo = length . filter ((1<) . snd) . M.toList

toLine :: Segment -> [Coord]
toLine (Segment (x1,y1) (x2,y2))
    | x1 == x2 = let (b, e) = minMax y1 y2 in (x1,) <$> [b..e]
    | y1 == y2 = let (b, e) = minMax x1 x2 in (,y1) <$> [b..e]
    | otherwise = []

toLine2 :: Segment -> [Coord]
toLine2 (Segment (x1,y1) (x2,y2))
    | x1 == x2 = let (b, e) = minMax y1 y2 in (x1,) <$> [b..e]
    | y1 == y2 = let (b, e) = minMax x1 x2 in (,y1) <$> [b..e]
    | otherwise = let
        dx = if x2 >= x1 then 1 else -1
        dy = if y2 >= y1 then 1 else -1
        in zip [x1, x1 + dx .. x2] [y1, y1 + dy .. y2]

---------- megaparsec stuff
type Parser = Parsec Void String

parseSegment :: Parser Segment
parseSegment = do
    x1 <- read <$> some digitChar
    char ','
    y1 <- read <$> some digitChar
    string " -> "
    x2 <- read <$> some digitChar
    char ','
    y2 <- read <$> some digitChar
    pure $ Segment (x1, y1) (x2, y2)
