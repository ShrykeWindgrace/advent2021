module Day23 (runDay) where
import           Paths_advent2021           (getDataFileName)
runDay :: IO ()
runDay = do
  realpath <- getDataFileName "input_day23.txt"
  content <- readFile realpath
  putStrLn "not implemented"
