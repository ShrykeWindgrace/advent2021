{-# LANGUAGE TemplateHaskell #-}
module Day2 (runDay) where
import           Control.Monad.State.Strict (State, execState)
import           Lens.Micro.Mtl             (use, (+=), (-=))
import           Lens.Micro.TH              (makeLenses)
import           Paths_advent2021           (getDataFileName)

data S = S {_depth :: Int, _forw :: Int} deriving Show
data S2 = S2 {_depth2 :: Int, _forw2 :: Int, _aim :: Int} deriving Show
makeLenses ''S
makeLenses ''S2

runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day02.txt"
    content <- readFile realpath
    let (Just inp) = traverse parse (lines content)
    let s@(S d z) = execState (traverse tick inp) (S 0 0)
    print s
    print $ d * z
    putStrLn "second part"
    let s2@(S2 d2 z2 _) = execState (traverse tick2 inp) (S2 0 0 0)
    print s2
    print $ d2 * z2

data Command = Up Int | Down Int | Forward Int

tick :: Command -> State S ()
tick (Up x)      = depth -= x
tick (Down x)    = depth += x
tick (Forward y) = forw += y

tick2 :: Command -> State S2 ()
tick2 (Up x) = aim -= x
tick2 (Down x) = aim += x
tick2 (Forward x) = do
    forw2 += x
    a <- use aim
    depth2 += a * x




parse :: String -> Maybe Command
parse s = case words s of
    ["up", s]      -> Just (Up $ read s)
    ["down", s]    -> Just (Down $ read s)
    ["forward", s] -> Just (Forward $ read s)
    _              -> Nothing
