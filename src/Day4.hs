{-# LANGUAGE OverloadedLists  #-}
{-# LANGUAGE TupleSections    #-}
{-# LANGUAGE TypeApplications #-}
module Day4 (runDay) where
import           Data.Char        (isDigit, isSpace)
import           Data.List.Extra  (splitOn, wordsBy)
import           Data.Maybe       (mapMaybe)
import           Data.Set         (member)
import           Paths_advent2021 (getDataFileName)
runDay :: IO ()
runDay = do
  realpath <- getDataFileName "input_day4.txt"
  content <- readFile realpath
  print 1 
