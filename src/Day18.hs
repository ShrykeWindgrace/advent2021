{-# LANGUAGE DerivingStrategies #-}
module Day18 where
import           Control.Monad.Combinators.Expr (Operator (InfixL),
                                                 makeExprParser)
import           Data.Void                      (Void)
import           Paths_advent2021               (getDataFileName)
import           Text.Megaparsec                (ParseErrorBundle, Parsec,
                                                 between, choice, oneOf,
                                                 runParser, some)
import           Text.Megaparsec.Char           (char, digitChar, string)

runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day18.txt"
    content <- readFile realpath
    print 1
