module Day6 (runDay) where
import           Control.Monad     (replicateM_)
import           Data.Array.IO     (IOUArray, MArray (newArray), readArray,
                                    writeArray, getAssocs)
import           Data.Array.MArray (MArray (newArray), readArray, writeArray, getElems, getAssocs)
import           Data.Foldable     (traverse_)
import           Data.List.Extra   (group, sort, wordsBy)
import qualified Data.Map.Strict   as M
import           Data.Maybe        (fromMaybe)
import           GHC.Word          (Word64)
import           Paths_advent2021  (getDataFileName)

runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day06.txt"
    content <-  wordsBy (== ',' ) <$> readFile realpath

    print $ count $ (!! 18) $ iterate tickMap $ toMap [3,4,3,1,2]
    print $ count $ (!! 80) $ iterate tickMap $ toMap [3,4,3,1,2]

    let initial = fmap read content :: [Int]
    print $ count $ (!! 80) $ iterate tickMap $ toMap initial
    print $ count $ (!! 256) $ iterate tickMap $ toMap initial

    putStrLn "with io arrays"
    
    let prep = fmap (\x -> (head x, length x)) $ group $ sort initial

    ioA <- newArray (0,8) 0  :: IO A
    traverse_ (\(t, q) -> writeArray ioA t (fromIntegral q)) prep
    getAssocs ioA >>= print

    replicateM_ 256 (tickA ioA)
    sumA ioA >>= print

type S = M.Map Int Int

tickMap :: S -> S
tickMap s = let
    asList = [ (t - 1, q) | (t, q) <- M.toList s, t > 0]
    mZ = fromMaybe 0 $ M.lookup 0 s

    in  M.insertWith (+) 6 mZ $ M.insertWith (+) 8 mZ $ M.fromList asList

count :: S -> Int
count = M.foldr' (+) 0

toMap :: [Int] -> S
toMap ls = M.fromList $ fmap (\x -> (head x, length x)) $ group $ sort ls


-- with mutable arrays
type A = IOUArray Int Word64

tickA :: A -> IO ()
tickA a = do
    zero <- readArray a 0
    traverse_ (\i -> readArray a (i+1) >>= writeArray a i) [0..7]
    writeArray a 8 zero
    six <- readArray a 6
    writeArray a 6 $ zero + six

sumA :: A -> IO Word64
sumA a = sum <$> getElems a
