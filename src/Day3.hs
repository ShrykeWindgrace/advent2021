{-# LANGUAGE StrictData    #-}
module Day3  where
import Control.Arrow ( (>>>) )
import           Paths_advent2021         (getDataFileName)
import Data.List (transpose)
import Data.Semigroup (Sum (Sum))

{-
Solutions are quite probably not algorithmically efficient, nor these approaches can be seen as different cases of one algorithm.

Yet they work and produce correct results, so I am happy for now
-}
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day03.txt"
    content <- lines <$> readFile realpath
    let (Just arr) = traverse (traverse parse) content
    let symbs = fmap parse' <$> arr
    let tr = transpose symbs
    let repr1 = best . foldr1 (<>) <$> tr
    let repr2 = fflip repr1

    print $ asInt repr1 * asInt repr2
    putStrLn "part 2"
    let v1 = asInt $ head $ mosts (length $ head arr) arr
    let v2 = asInt $ head $ leasts (length $ head arr) arr
    print $ v1 * v2

partitionMostAt ::  Int -> [[Symb]] -> [[Symb]]
partitionMostAt _ [x] = [x]  -- no filtering if only one line remains
partitionMostAt p xs = let
    zs = filter (\l -> l!!p == Zero) xs
    os = filter (\l -> l!!p == One) xs
    in if length zs > length os then zs else os

partitionLeastAt ::  Int -> [[Symb]] -> [[Symb]]
partitionLeastAt _ [x] = [x]  -- no filtering if only one line remains
partitionLeastAt p xs = let
    zs = filter (\l -> l!!p == Zero) xs
    os = filter (\l -> l!!p == One) xs
    in if length os < length zs then os else zs


mosts :: Int -> [[Symb]] -> [[Symb]]
mosts len = foldr (>>>) id $ fmap partitionMostAt [0..len - 1]

leasts :: Int -> [[Symb]] -> [[Symb]]
leasts len = foldr (>>>) id $ fmap partitionLeastAt [0..len - 1]

data Symb = Zero | One deriving (Show, Eq)
data Counter = Counter {_zero :: Sum Int, _one:: Sum Int} deriving Show

instance Semigroup Counter where
    Counter z1 o1 <> Counter z2 o2 = Counter (z1 <> z2) (o1 <> o2)

instance Monoid Counter where
    mempty = Counter (Sum 0) (Sum 0)

best :: Counter -> Symb
best (Counter (Sum z) (Sum o))
    | z > o = Zero
    | z < o = One
    | otherwise  = error "logic error; assume safe input"

parse :: Char -> Maybe Symb
parse '1' = Just One
parse '0' = Just Zero
parse _ = Nothing

parse' :: Symb -> Counter
parse' One = Counter (Sum 0) (Sum 1)
parse' Zero = Counter (Sum 1) (Sum 0)


-- | Read as binary, big endian
asInt :: [Symb] -> Int
asInt = foldl upd 0 where
    upd acc One  = 2 * acc + 1
    upd acc Zero  = 2 * acc

-- | Replace 1 with 0 and vice versa 
fflip :: [Symb] -> [Symb]
fflip = fmap g where
    g One = Zero
    g Zero = One