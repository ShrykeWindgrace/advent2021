module Day17 (runDay) where
import           Data.List.Extra (scanl')
import           Data.Tuple      (swap)

runDay :: IO ()
runDay = do
    print part1
    print part2

validX :: Int -> Bool
validX x = 144 <= x && x <= 178

unboundedX :: Int ->[Int]
unboundedX x = scanl' (+) 0 $ [x, x - 1 .. 0] <> repeat 0

unboundedY :: Int -> [Int]
unboundedY y = scanl' (+) 0 [y, y - 1 ..]

limY :: Int
limY = 101

test :: (Int, Int) -> Bool
test (x, y) = any (validX . fst) $ dropWhile ((> -76) . snd ) $ takeWhile ((>= -100) . snd ) $ zip (unboundedX x) (unboundedY y)

testBed :: [(Int, Int)]
testBed = swap <$> ( (,) <$> [101, 100 .. 0] <*> [16..178] ) -- like this we test larger y first

testBed2:: [(Int, Int)]
testBed2 = (,) <$> [16..178] <*> [-101..101]

part1 :: Int
part1 = (\y -> (y * y + y) `quot` 2) $ head $ snd <$> filter test testBed

part2 :: Int
part2 = length $ filter test testBed2 -- that's almost 1Gb of allocs, but whatever
