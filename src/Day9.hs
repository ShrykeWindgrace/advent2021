module Day9 (runDay) where
import           Algebra.Graph         (Graph, empty, overlay, star, tree,
                                        vertexCount)
import           Algebra.Graph.ToGraph (ToGraph (dfsForest))
import           Data.Char             (digitToInt)
import           Data.Function         ((&))
import           Data.Functor          ((<&>))
import           Data.List             (sortOn)
import qualified Data.Map.Strict       as M
import           Data.Maybe            (mapMaybe)
import           Data.Ord              (Down (Down))
import           Data.Semigroup        (Sum (Sum, getSum))
import           Paths_advent2021      (getDataFileName)

runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day09.txt"
    content <- readFile realpath
    print $ getSum $ part1 $ parse content
    parse content
        & asGraph
        & basins <&> unwrap
        <&> vertexCount
        & sortOn Down & take 3 -- three largest
        & product & print

type Grid = M.Map (Int, Int) Int

-- | don't look too closely, it works
parse :: String -> Grid
parse s = s & lines <&> zip [0..] & zip [0..] >>= shuffle <&> fmap digitToInt & M.fromList where

    shuffle :: (Int, [(Int, a)]) -> [((Int,Int), a)]
    shuffle (x, ps) = fmap (\(y, a) -> ((x,y),a)) ps

-- | detect local minima
part1 :: Grid -> Sum Int
part1 g = flip M.foldMapWithKey g $ \ (x,y) v -> let
    adj = mapMaybe (`M.lookup` g) [(x-1,y), (x+1,y), (x,y-1), (x,y+1)]
    in
        if all (v <) adj then Sum (v + 1) else mempty



instance Semigroup WrapGraph where
    WrapGraph x <> WrapGraph y = WrapGraph $ x `overlay` y
instance Monoid WrapGraph where
    mempty  = WrapGraph empty

newtype WrapGraph = WrapGraph {unwrap:: Graph (Int, Int)}

-- | Build adjacency graph from the grid; drop all '9'
asGraph :: Grid -> WrapGraph
asGraph g = flip M.foldMapWithKey g $ \ k@(x,y) v -> let
    adj = [k | k <- [(x-1,y), (x+1,y), (x,y-1), (x,y+1)], maybe False (/= 9) (M.lookup k g) ]
    in if v == 9 then mempty else WrapGraph (star k adj)

-- | workhorse for part2 - it detects connected graph components
basins :: WrapGraph -> [WrapGraph]
basins (WrapGraph gr) = dfsForest gr <&> tree <&> WrapGraph

