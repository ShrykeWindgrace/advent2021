{-# LANGUAGE TypeApplications #-}
module Day15 where
import           Control.Monad.State.Strict (State, evalState, execState, get,
                                             replicateM_)
import qualified Data.IntMap.Strict         as IM
import           Lens.Micro                 (_1, _2, _3, (^.))
import           Lens.Micro.Mtl             (use, (%=), (+=), (.=))
import           Control.DeepSeq

-- to be run with, e.g.
-- $ stack exec -- advent --day 15 +RTS -s -A128m -AL256m -qn8 -N12 -I0
--
-- 30Gb total heap allocs, ~23s total time
runDay :: IO ()
runDay = do
    print 1
