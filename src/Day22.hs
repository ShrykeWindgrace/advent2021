module Day22 (runDay) where
import           Control.Monad.State.Strict (State, evalState)
import           Data.Monoid                (Sum (Sum))
import           Data.Semigroup             (Sum (getSum))
import           Data.Sequence
import           Lens.Micro
import           Lens.Micro.Mtl
import           Paths_advent2021           (getDataFileName)
import           Prelude                    hiding (drop, reverse)
import qualified Data.List
import Data.List.Extra (splitOn)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day22.txt"
    content <- readFile realpath
    print 1
