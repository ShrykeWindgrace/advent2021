module Day14 (runDay) where
import           Control.Monad.RWS.Strict (MonadReader (ask), RWS, execRWS,
                                           replicateM)
import           Data.List.Extra          (group, sort, splitOn)
import qualified Data.Map.Strict          as M
import           Data.Semigroup           (Max (Max), Min (Min))
import           Lens.Micro               (_1, _2)
import           Lens.Micro.Mtl           (assign, use, (%=), (.=))
import           Paths_advent2021         (getDataFileName)

runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day14.txt"
    content <- readFile realpath
    let (ini, r, s) = parseInput content

    print $ minMax $ freqs $ (!! 10) $ iterate (tick r) ini

    print $ answer $ snd $ fst $ execRWS (replicateM 10 tick2) r (initM ini, s)
    print $ answer $ snd $ fst $ execRWS (replicateM 40 tick2) r (initM ini, s)


tick :: Reactions -> String -> String
tick _ [] = []
tick _ [z] = [z]
tick m (x:y:zs) = case M.lookup (x,y) m of
    Nothing -> x : tick m (y:zs)
    Just c  -> x : c : tick m (y:zs)

type Reactions = M.Map (Char, Char) Char
type PairFreqs = M.Map (Char, Char) Int
type CharFreqs = M.Map Char Int

answer :: CharFreqs -> Int
answer = minMax . M.elems

type W a = RWS Reactions () (PairFreqs, CharFreqs) a


initM :: String -> PairFreqs
initM s = M.fromList $ fmap (\p -> (head p, length p)) $ group $ sort $ zip s (tail s)

tick2 :: RWS Reactions () (PairFreqs, CharFreqs) ()
tick2 = use _1 >>= buildNewFreqs >>= assign _1

buildNewFreqs :: PairFreqs -> W PairFreqs
buildNewFreqs = M.foldrWithKey' oneFreq (pure M.empty)

oneFreq  :: (Char, Char) -> Int -> W PairFreqs -> W PairFreqs
oneFreq p@(x,y) count acc = do
    r <- ask
    case M.lookup p r of
        Nothing -> upd p count <$> acc
        Just c -> do
            _2 %= upd c count
            upd (x, c) count . upd (c, y) count <$> acc

freqs :: String -> [Int]
freqs = fmap length . group . sort

minMax :: [Int] -> Int
minMax xs = let (Min x, Max y) = foldMap (\z -> (Min z, Max z)) xs in y - x

upd :: (Ord k, Num a) => k -> a -> M.Map k a -> M.Map k a
upd = M.insertWith (+)

parseRepl :: String -> ((Char,Char), Char)
parseRepl s = let [[x,y], [sy]] = splitOn " -> " s in  ((x, y), sy)

parseInput :: String -> (String, Reactions, CharFreqs)
parseInput s = let [ini, remaining] = splitOn "\n\n" s in
    (   ini,
        M.fromList $ parseRepl <$> lines remaining,
        M.fromList $ fmap (\g -> (head g, length g)) $ group $ sort ini)

