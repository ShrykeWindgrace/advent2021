{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Day21  where
import           Control.Applicative.Combinators (sepEndBy1)
import           Control.Monad.State.Strict      (State, execState, unless)
import           Data.Coerce                     (coerce)
import           Data.List                       (delete, foldr, intercalate,
                                                  intersect, sortOn)
import qualified Data.Map.Strict                 as Map
import qualified Data.Set                        as Set
import           Data.Void                       (Void)
import           Lens.Micro                      (_1, _2)
import           Lens.Micro.Mtl                  (use, (%=))
import           Paths_advent2021                (getDataFileName)
import           Text.Megaparsec                 (ParseErrorBundle, Parsec,
                                                  between, parseMaybe, some)
import           Text.Megaparsec.Char            (char, letterChar, space,
                                                  string)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day21.txt"
    content <- readFile realpath
    print 1
