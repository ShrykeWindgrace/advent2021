module Utils (limit, propagate) where
import Data.Foldable (Foldable(fold))


limit :: (Eq a) => (a -> a) -> a -> a
limit step init = let next = step init in if init == next then init else limit step next

propagate :: (Applicative f, Traversable t, Monoid m) => (a -> f m) -> t a -> f m
propagate g = fmap fold . traverse g