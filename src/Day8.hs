{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE StrictData        #-}
module Day8 (runDay) where
import           Control.Monad.RWS.Strict (MonadReader (ask), MonadState (get),
                                           RWST, asks, execRWST, gets, modify)
import           Data.List                (sort, (\\))
import qualified Data.Map.Strict          as M
import           Data.Set                 (Set, empty, insert, member)
import           Paths_advent2021         (getDataFileName)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day08.txt"
    content <- lines <$> readFile realpath
    print $ sum $ fmap getOut content

    let r = sum <$> traverse decode content
    print r

unique :: [Int]
unique = [2,3,4,7]

getOut :: String -> Int
getOut s = length $ filter ( `elem` unique) $ fmap length $ words $ dropWhile (/= '|') s


type S a = RWST [String] () (M.Map Char String) (Either String)  a


chain :: S ()
chain = mk1478 >> detectA >> detect9GE60 >> detectD >> detectB >> detectRest

mk1478 :: S ()
mk1478 = do
    coded <- ask
    let codedOne = head $ filter ((2 ==) . length)  coded
    let codedSeven = head $ filter ((3 ==) . length)  coded
    let codedFour = head $ filter ((4 ==) . length) coded
    let codedEight = head $ filter ((7 ==) . length) coded
    modify $ M.insert '1' codedOne
    -- lift $ print codedOne
    modify $ M.insert '4' codedFour
    -- lift $ print codedFour
    modify $ M.insert '7' codedSeven
    -- lift $ print codedSeven
    modify $ M.insert '8' codedEight
    -- lift $ print codedEight

detectA :: S ()
detectA = do
    seven_ <- getC '7'
    one_ <- getC '1'
    let a = (\\) seven_ one_
    modify $ M.insert 'a' a

detect9GE60 :: S ()
detect9GE60 = do
    zero_six_nine <- asks $ filter ((6==) . length)
    four <- getC '4'
    let nine = head $ filter (four `subl`) zero_six_nine
    modify $ M.insert '9' nine
    eight <- getC '8'
    let e = eight \\ nine
    modify $ M.insert 'e' e
    a <- getC 'a'
    let g = (nine \\ four) \\ a
    modify $ M.insert 'g' g

    one <- getC '1'

    let six = head $ filter (not . (one `subl`)) zero_six_nine
    modify $ M.insert '6' six
    let zero = head [z | z<- zero_six_nine, z /= six, z /= nine]

    modify $ M.insert '0' zero


detectD :: S ()
detectD = do
    eight <- getC '8'
    zero <- getC '0'
    let d = eight \\ zero
    modify $ M.insert 'd' d

detectB :: S ()
detectB = do
    zero <- getC '0'
    e <- getC 'e'
    g <- getC 'g'
    seven <- getC '7'
    let b = ((zero \\ e) \\ g) \\ seven
    modify $ M.insert 'b' b

detectRest :: S ()
detectRest = do
    two_three_five <- asks $ filter ((5==) . length)
    one <- getC '1'
    let three = head $ filter (one `subl`) two_three_five
    b <- head <$> getC 'b'
    let five = head $ filter (b `elem`) two_three_five
    let two = head $ [z | z <- two_three_five, z /= three, z /= five]
    modify $ M.insert '2' two
    modify $ M.insert '3' three
    modify $ M.insert '5' five

subl :: Eq a => [a] -> [a] -> Bool
hay `subl` needle = all (`elem` needle) hay

getC :: Char -> S String
getC c = do
    s <- gets (M.lookup c)
    case s of
        Just x  -> pure x
        Nothing -> fail ("query failed for " <> show c)

instance MonadFail (Either String) where
    fail = Left

buildReverse :: M.Map Char String -> M.Map String Char
buildReverse m = let
    l = [(sort code, val) | (val, code) <- M.toList m, val `elem` "0123456789"]
    in M.fromList l

decode :: String -> Either String Int
decode s = let
    c = words $ takeWhile ('|' /= ) s
    v = fmap sort $ words $ drop 1 $ dropWhile ('|' /= ) s
    est = fst <$> execRWST chain c M.empty
    in
    buildReverse <$> est >>= (\b -> do
        case read <$> traverse (`M.lookup` b) v of
                        Nothing -> fail "map incomplete"
                        Just s  -> pure s
    )
