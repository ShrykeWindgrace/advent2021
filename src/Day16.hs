{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE StrictData         #-}
module Day16 (runDay) where
import           Control.Applicative      (Applicative (liftA2))
import           Control.Monad.Extra      (replicateM)
import           Control.Monad.RWS.Strict (MonadWriter (tell), RWS, Sum (Sum),
                                           replicateM, runRWS)
import           Data.List.Extra          (foldl', product)
import           Lens.Micro               (_1, _2, (&))
import           Lens.Micro.Mtl           (use, (-=), (<<%=))
import           Paths_advent2021         (getDataFileName)

runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day16.txt"
    content <- readFile realpath
    print $ eval content

type Input = [Int]
type Band a = RWS () (Sum Int) (Input, Int) a

eval :: String -> (Int, Int)
eval s = s
    & toBits
    & (\z -> runRWS parsePacket () (z, 0))
    &  (\(z,_, Sum x) -> (x, z))

parsePacket :: Band Int
parsePacket = do
    getN 3 >>= (tell . Sum)  -- version
    typeId <- getN 3
    if typeId == 4 then getLiteral else do
        let fn = getFn typeId
        lengthTypeId <- getN 1
        if lengthTypeId == 1 then do
            count <- getN 11
            fn <$> replicateM count parsePacket
        else do
            bitLength <- getN 15
            subBand <- cutN bitLength
            let (packs ,_, z) = runRWS parseWhile () (subBand, bitLength)
            tell z
            pure $ fn packs


parseWhile :: Band [Int]
parseWhile = do
    rems <- (0<) <$> use _2
    if rems then liftA2 (:) parsePacket parseWhile else pure []


cutN :: Int -> Band Input
cutN n = do
    old <- _1 <<%= drop n
    _2 -= n
    pure $ take n old

getN :: Int -> Band Int
getN = fmap readInt . cutN


cutLiteral :: Band Input
cutLiteral = do
    ht <- cutN 5
    case ht of
        []    -> pure [] -- should never happen
        (h:t) -> if h == 0 then pure t else (t <>) <$> cutLiteral

getLiteral :: Band Int
getLiteral = readInt <$> cutLiteral


readInt :: Input -> Int
readInt = foldl' (\acc v -> v + 2 * acc) 0


-- | a lot of incomplete pattern matches. We do not care
getFn :: Int -> ([Int] -> Int)
getFn 0 = sum
getFn 1 = product
getFn 2 = minimum
getFn 3 = maximum
getFn 5 = (\[l,r] -> if l > r then 1 else 0)
getFn 6 = (\[l,r] -> if r > l then 1 else 0)
getFn 7 = (\[l,r] -> if l == r then 1 else 0)


toBits :: String -> Input
toBits = (>>= go) where
    go :: Char -> Input
    go '0' = [0,0,0,0]
    go '1' = [0,0,0,1]
    go '2' = [0,0,1,0]
    go '3' = [0,0,1,1]
    go '4' = [0,1,0,0]
    go '5' = [0,1,0,1]
    go '6' = [0,1,1,0]
    go '7' = [0,1,1,1]
    go '8' = [1,0,0,0]
    go '9' = [1,0,0,1]
    go 'A' = [1,0,1,0]
    go 'B' = [1,0,1,1]
    go 'C' = [1,1,0,0]
    go 'D' = [1,1,0,1]
    go 'E' = [1,1,1,0]
    go 'F' = [1,1,1,1]

