{-# LANGUAGE DerivingStrategies #-}
module Day10 (runDay, toDec, parseTest) where
import           Control.Monad.State.Strict (MonadState (get), State, execState,
                                             when)
import           Data.List                  (foldl', sort)
import           Paths_advent2021           (getDataFileName)
import           Lens.Micro                 (_1, _2)
import           Lens.Micro.Mtl             ((%=), (.=))
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day10.txt"
    content <- lines <$> readFile realpath
    let Just r = traverse getOutcome content
    print $ sum $ [cost z | (Illegal z, _) <- r]

    let p2 = [toDec s | (GoOn, s) <- r]

    let len = length p2
    print $ sort p2 !! (len `quot` 2)

data S =
    Parens | -- ()
    Bracket | -- []
    Brace | -- {}
    Angle  -- <>
    deriving stock Eq

data D = Open | Close deriving stock Eq

cost :: S -> Int
cost  Parens  = 3
cost  Bracket = 57
cost  Brace   = 1197
cost  Angle   = 25137

data Token = Token S D

parse :: Char -> Maybe Token
parse '(' = Just $ Token Parens Open
parse ')' = Just $ Token Parens Close
parse '[' = Just $ Token Bracket Open
parse ']' = Just $ Token Bracket Close
parse '{' = Just $ Token Brace Open
parse '}' = Just $ Token Brace Close
parse '<' = Just $ Token Angle Open
parse '>' = Just $ Token Angle Close
parse _   = Nothing

data Outcome = GoOn | Illegal S deriving stock Eq

tick :: Token -> State (Outcome, [S]) ()
tick t@(Token s d) = do
    (o, stack) <- get
    when (o == GoOn) $ do
        case stack of
            [] -> if d == Close then _1 .= Illegal s else _2 .= [s]
            (s_ : _) -> do
                let action | s == s_ && d == Close = _2 %= tail
                           | d == Open = _2 %= (s:)
                           | otherwise = _1 .= Illegal s
                action

getOutcome :: String -> Maybe (Outcome, [S])
getOutcome s =  flip execState (GoOn, []) . traverse tick <$> traverse parse s

toDec :: [S] -> Int
toDec = foldl' (\acc s -> 5 * acc + weight s) 0 where
    weight :: S -> Int
    weight Parens  = 1
    weight Bracket = 2
    weight Brace   = 3
    weight Angle   = 4

parseTest :: String -> Maybe [S]
parseTest s = fmap (\(Token s _) -> s) <$> traverse parse s

