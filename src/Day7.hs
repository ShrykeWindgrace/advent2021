module Day7 (runDay) where

import           Data.List.Extra  (wordsBy)

import           Data.List        (sort)
import           Paths_advent2021 (getDataFileName)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day07.txt"
    content <- readFile realpath
    let ints = read <$> wordsBy (',' ==) content
    let goal = argMin1 ints
    print $ cost1 goal ints

    putStrLn "it is sad that such bruteforce solutions work fast enough:"

    print $ bruteForce (\x y -> abs $ x - y) ints
    print $ bruteForce weight2 ints

argMin1 :: [Int] -> Int
argMin1 [] = 0
argMin1 xs = let l = length xs in sort xs !! (l `quot` 2)


cost1 :: Int -> [Int] -> Int
cost1 g xs = sum (abs . (g-) <$> xs)

weight2 :: Int -> Int -> Int
weight2 x y = (abs (x-y) * (abs (x-y) + 1)) `quot` 2

bruteForce :: (Int -> Int -> Int) -> [Int] -> Int
bruteForce w xs = let
    l = minimum xs
    r = maximum xs
    coster g = sum (w g <$> xs)
    in minimum $ coster <$> [l..r]
