module Day24 (runDay) where
import           Data.Set             (Set)
import qualified Data.Set             as Set
import           Data.Void            (Void)
import           Lens.Micro           (_1, _2, over)
import           Paths_advent2021     (getDataFileName)
import           Text.Megaparsec      (ParseErrorBundle, Parsec, between,
                                       choice, oneOf, parse, runParser, some)
import           Text.Megaparsec.Char (char, digitChar, string)

runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day24.txt"
    content <- readFile realpath
    print 1
