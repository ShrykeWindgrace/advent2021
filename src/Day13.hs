module Day13 (runDay) where
import           Control.Arrow    ((>>>))
import           Data.Foldable    (traverse_)
import           Data.Function    ((&))
import           Data.List.Extra  (groupBy, sort, splitOn, transpose)
import           Data.Semigroup   (Max (Max), Min (Min))
import           Data.Set         (Set)
import qualified Data.Set         as S
import           Lens.Micro       (ix, (.~))
import           Paths_advent2021 (getDataFileName)


runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day13.txt"
    content <-  readFile realpath
    let Just (cs, comms) = parseInput content
    let initS = S.fromList cs
    print $ S.size $ fold (head comms) initS
    let res2 = foldr1 (>>>) (fmap fold comms) initS
    traverse_ putStrLn $ pretty res2

type Coord  = (Int, Int)

data Command  = X Int | Y Int -- folds


fold :: Command -> Set Coord -> Set Coord
fold c = S.foldr ( S.insert . select c ) S.empty where
    select :: Command -> Coord -> Coord
    select (X f) (x,y) = (if x < f then x else 2 * f - x, y)
    select (Y f) (x,y) = (x, if y < f then y else 2 * f - y)


pretty :: Set Coord -> [String]
pretty s = let
    (Min minX, Min minY, Max maxX, Max maxY) = S.foldr (\(x,y) acc -> (Min x, Min y, Max x, Max y) <> acc) mempty s
    start = replicate (maxY - minY + 1) $ replicate (maxX - minX + 1) ' '
    in S.foldr (\(x,y) acc -> acc & ix (y - minY) . ix (x - minX)  .~ '#') start s -- that's on hell of inefficiency...


parseCommand :: String -> Maybe Command
parseCommand s = s & words & (!!2) & detect where
    detect :: String -> Maybe Command
    detect ('x':'=':r) = Just $ X $ read r
    detect ('y':'=':r) = Just $ Y $ read r
    detect _           = Nothing


parseInput :: String -> Maybe ([Coord], [Command])
parseInput s = let
    [coords, comms_] = splitOn "\n\n" s
    cs = parseCoord <$> lines coords
    comms = traverse parseCommand $ lines comms_
    in
        sequence (cs, comms)

parseCoord :: String -> Coord
parseCoord s = let [x,y] = splitOn "," s in (read x, read y)
