{-# LANGUAGE StrictData #-}
module Day12 (runDay) where
import           Algebra.Graph    (Context (Context), Graph, context, edges)
import           Data.Char        (isLower)
import           Data.List.Extra  (nub, splitOn)
import qualified Data.Map.Strict  as M
import           Paths_advent2021 (getDataFileName)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day12.txt"
    content <- lines <$> readFile realpath
    let gr = edges $ parseConnection <$> content
    print $ length $ run gr [] [] Start

    print $ length $ run2 gr M.empty [] Start

data Cave = Start | End | L String | U String deriving (Eq, Show, Ord)

isSingleUse :: Cave -> Bool
isSingleUse (U _) = False
isSingleUse _     = True

type G = Graph Cave

run :: G -> [Cave] -> [Cave] -> Cave -> [[Cave]]
run g seen path current
    | current `elem` seen = []
    | current == End = [End:path]
    | otherwise = case context (== current) g of
        Nothing -> []
        Just (Context i o) -> let
            ns = nub (i <> o)
            nSeen = if isSingleUse current then current:seen else seen
            in
                concat $ run g nSeen (current:path) <$> ns

-- True if can go on
logic :: Cave -> M.Map String Int -> (M.Map String Int, Bool)
logic (U _) m = (m, True)
logic End m = (m, False)
logic Start m = if "start" `M.member` m then (m, False) else (M.insert "start" 1 m, True)
logic (L s) m = case M.lookup s m of
    Nothing -> (M.insert s 1 m, True)
    Just 1  -> if hasTwice m then (m, False) else (M.insert s 2 m, True)
    _       -> (m, False)

hasTwice :: M.Map String Int -> Bool
hasTwice m = not $ null $ [ v | (v, 2) <- M.toList m ]


run2 :: G -> M.Map String Int -> [Cave] -> Cave -> [[Cave]]
run2 g seen path current = case logic current seen of
    (nSeen, False) -> [End:path | End == current]
    (nSeen, True) -> case context (== current) g of
        Nothing -> []
        Just (Context i o) -> let
            ns = nub (i <> o)

            in
                concat $ run2 g nSeen (current:path) <$> ns


parseConnection :: String -> (Cave, Cave)
parseConnection s = let
    [f,t] = splitOn "-" s
    in (p f, p t) where
        p :: String -> Cave
        p "start" = Start
        p "end"   = End
        p z       = if all isLower z then L z else U z

