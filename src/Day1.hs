module Day1 (runDay) where
import           Paths_advent2021 (getDataFileName)
runDay :: IO ()
runDay = do
  realpath <- getDataFileName "input_day01.txt"
  content <- readFile realpath
  let ints = read <$> lines content
  putStrLn "First part: "
  print $ increases ints
  putStrLn "Second part: "
  print $ increases $ threes ints

  putStrLn "Generalized solution"
  print $ sol 1 ints
  print $ sol 3 ints


increases :: [Int] -> Int
increases xs = length $ filter (0 <) $ zipWith (-) (tail xs) xs

threes :: [Int] -> [Int]
threes xs = zipWith3 (\a b c -> a + b + c) xs (tail xs) (tail (tail xs))

------- via drop ---------

sol :: Int -> [Int] -> Int 
sol dr xs = length $ filter (0 <) $ zipWith (-) (drop dr xs) xs